import React, { useState } from 'react';
import './App.css';
import Login from './components/login/Login';
import Translate from './components/translate/Translate';
import Profile from './components/profile/Profile';
import AppHeader from './components/AppHeader';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

function App() {

  const [isLoggedIn, setIsLoggedIn] = useState(false);


  const handleUserLogin = () => {
    setIsLoggedIn(true);
  }
  return (
    <Router>
      <div className='App'>
        <AppHeader userLoggedIn={isLoggedIn} />
        <Switch>
          <Redirect exact from="/" to="/login" />
          <Route path="/login">
            <Login userLoggedIn={handleUserLogin} />
          </Route>
          <Route path="/translate" component={Translate} />
          <Route path="/profile" component={Profile} />
          <Route path="*" component={Login} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
