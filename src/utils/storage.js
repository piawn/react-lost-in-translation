const initalHistoryData = {
  results: [],
};

export const setStorage = (key, value) => {
  const json = JSON.stringify(value);
  localStorage.setItem(key, json);
};

export const getStorage = (key) => {
  const storedValue = localStorage.getItem(key);
  if (storedValue) {
    return JSON.parse(storedValue);
  }
  return false;
};

export const deleteStorage = (key) => {
  localStorage.removeItem(key);
};

export const initHistoryStorage = (key) => setStorage(key, initalHistoryData);
