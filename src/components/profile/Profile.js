import React, { useState, useEffect } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { getStorage, deleteStorage } from '../../utils/storage';
import TranslateOutput from '../translate/TranslateOutput';
import '../common/card.css';
import './Profile.css';
import { ReactComponent as DeleteIcon } from '../../assets/delete.svg';
import { ReactComponent as ArrowBack } from '../../assets/arrow-back.svg';


const Profile = (props) => {

  const [isLoggedIn, setIsLoggedIn] = useState(true);
  const [isHistory, setIsHistory] = useState(true);

  useEffect(() => {
    setIsLoggedIn(getStorage('user'));
  }, []);

  useEffect(() => {
    setIsHistory(getStorage('translated'));
  }, []);

  const { history } = getStorage('translated');
  let historyList;

  console.log('History: ' + history);
  console.log('getStorage: ' + getStorage('translated'));
  console.log('isHistory: ' + isHistory);

  if (history) {
    historyList = history.map((word, i) => {
      return (
        <div className="card" key={i}>
          <div className="card-header">
            {word}
          </div>
          <div className="card-content">
            <TranslateOutput translate={word}></TranslateOutput>
          </div>
          <div className="card-footer"></div>
        </div>
      );
    });
  }

  const onClearHistoryClicked = () => {
    deleteStorage('translated');
    setIsHistory(false);
  };

  return (
    <div>
      <div className="container">
        <div className="container-top">
          {!isLoggedIn && <Redirect to='/login' />}
          <Link to='/translate'>
            <ArrowBack className='icon-button arrow-back' />
          </Link>
          <div>
            <h1>Profile</h1>
            <p>Translation history</p>
          </div>
          {isHistory ? <DeleteIcon className='icon-button btn-delete' onClick={onClearHistoryClicked} /> : <span />}
        </div>
        {!isHistory &&
          <div className="card">
            <div className="card-content">
              <p>No translation history!</p>
            </div>
            <div className="card-footer"></div>
          </div>}
        {isHistory && historyList}
      </div>
    </div>
  );
};

export default Profile;
