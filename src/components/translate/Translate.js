import React, { useEffect } from 'react';
import { getStorage, setStorage } from '../../utils/storage';
import { Redirect } from 'react-router-dom';
import TranslateForm from './TranslateForm';
import TranslateOutput from './TranslateOutput';
import { useState } from 'react';
import '../common/card.css'

const Translation = (props) => {

  useEffect(() => {
    setIsLoggedIn(getStorage('user'));
  }, []);

  const [isLoggedIn, setIsLoggedIn] = useState(true);
  const [inputValue, setInputValues] = useState([]);
  const [newInput, setNewInput] = useState(false);

  const { name } = getStorage('user');
  console.log(name);

  const handleTranslateClicked = (input) => {
    setInputValues(input);
    setNewInput(true);
    storeInput(input);
    console.log('Translate ' + input);
    console.log(inputValue);
  };

  const storeInput = (newInput) => {
    let { history } = getStorage('translated');
    let newTranslateHistory = [newInput];

    if (!history) {
      setStorage('translated', { history: newTranslateHistory });
    } else if (history.length === 10) {
      history.pop();
      history.unshift(newInput);
      setStorage('translated', { history });
    } else if (history.length < 10) {
      history.unshift(newInput);
      setStorage('translated', { history });
    }
  };

  return (
    <div>
      <div className="container">
        {!isLoggedIn && <Redirect to='/login' />}
        <TranslateForm translateClicked={handleTranslateClicked} />
        <div className="card" >
          <div className="card-content">
            {newInput && <TranslateOutput translate={inputValue} />}
            {!newInput && <p>Write something to translate!</p>}
          </div>
          <div className="card-footer"></div>
        </div>
      </div>
    </div>
  );
};

export default Translation;

