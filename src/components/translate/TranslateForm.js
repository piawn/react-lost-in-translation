import React, { useState } from 'react';
import '../common/inputform.css';
import { ReactComponent as ChevronBtn } from '../../assets/chevron.svg';


export const TranslateForm = props => {

    const [input, setInput] = useState('');

    const onInputChanged = event => setInput(event.target.value.trim());

    const onTranslateClicked = () => {
        if (!validateInput()) {
            alert('Please input letters a to z');
        } else {
            props.translateClicked(input);
            console.log('TranslateForm ' + input);
        }
    };

    const validateInput = () => {
        var letters = /^[a-z A-Z]+$/;
        if (input.match(letters)) {
            return true;
        }
        return false;
    }

    return (
        <form className='container-form container-translate'>
            <input type="text" placeholder="Enter a word" onChange={onInputChanged} maxLength='40'></input>
            <ChevronBtn className='btn-enter' type="button" onClick={onTranslateClicked} />
        </form>
    )
};

export default TranslateForm;