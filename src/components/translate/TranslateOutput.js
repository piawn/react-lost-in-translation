import React from 'react';
import './TranslateOutput.css';

export const TranslateOutput = ({ translate }) => {

  const importAll = (r) => {
    let images = {};
    r.keys().forEach((item) => {
      images[item.replace('./', '')] = r(item);
    });
    return images;
  };
  const signs = importAll(
    require.context('../../assets/individual_signs', false, /\.(png)$/)
  );

  const translated = [...translate.toLowerCase()].map((sign, index) => {
    if (sign !== ' ') {
      return (
        <img
          className='img-sign'
          src={signs[`${sign}.png`]}
          alt={sign}
          key={index}
        />
      );
    } else {
      return (
        <span key={index}></span>
      )
    }
  });

  return <div className='translate-output-content'>{translated}</div>;
};

export default TranslateOutput;
