import React, { useState, useEffect } from 'react';
import LoginForm from './LoginForm';
import { setStorage, getStorage } from '../../utils/storage';
import { Redirect } from 'react-router-dom';
import './Login.css';
import '../common/card.css';

const Login = (props) => {

  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    setIsLoggedIn(getStorage('user'));
  }, []);

  const handleLoginClicked = (name) => {
    if (name) {
      setStorage('user', {
        name,
      });
      setIsLoggedIn(getStorage('user'));
      props.userLoggedIn();
    }
  };

  return (
    <div className='container container-small login-content'>
      {isLoggedIn && <Redirect to='/translate' />}
      <h1>Lost in translation</h1>
      <h2>Get started</h2>
      <div className="card">
        <div className="card-content">
          <LoginForm loginClicked={handleLoginClicked} />
        </div>
        <div className="card-footer" />
      </div>
    </div>
  );
};

export default Login;