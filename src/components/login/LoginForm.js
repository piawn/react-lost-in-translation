import React, { useState } from 'react';
import '../common/inputform.css';
import { ReactComponent as ChevronBtn } from '../../assets/chevron.svg';

const LoginForm = (props) => {
  const [name, setName] = useState('');

  const onNameChanged = (event) => setName(event.target.value.trim());

  const onLoginClicked = (event) => {
    if (!name) {
      alert('Please enter your name!');
    }
    props.loginClicked(name);
  };

  return (
    <form className='container-form'>
      <input
        type='text'
        placeholder="What's your name?"
        onChange={onNameChanged}
      ></input>
      <ChevronBtn className='btn-enter' type='button' onClick={onLoginClicked} />
    </form>
  );
};

export default LoginForm;
