import React, { useState, useEffect } from 'react';
import './AppHeader.css';
import { getStorage, deleteStorage } from '../utils/storage';
import { Link } from 'react-router-dom';
import LogoHello from '../assets/logo/Logo-Hello.png';
import { ReactComponent as Logout } from '../assets/logout.svg'
import { ReactComponent as ProfileIcon } from '../assets/profile-icon.svg';

const AppHeader = (props) => {

  const [isLoggedIn, setIsLoggedIn] = useState(true);
  const [name, setName] = useState('');

  useEffect(() => {
    const { name } = getStorage('user');
    setIsLoggedIn(name);
    setName(name);
  }, [props.userLoggedIn]);

  const onLogoutClicked = () => {
    deleteStorage('user');
    deleteStorage('translated');
    setIsLoggedIn(false);
  };

  return (
    <header className='app-header'>
      <div className='app-header-content'>
        <div className='header-left'>
          <img className='img-logo-hello' src={LogoHello} alt='LogoHello'></img>
          <h1 className='title'>Lost in translation</h1>
        </div>
        {isLoggedIn &&
          <div className='header-right'>
            {name}
            <Link to='/profile'>
              <ProfileIcon className='icon-button profile-button'></ProfileIcon>
            </Link>
            <Link to='/login'>
              <Logout className='icon-button btn-logout' onClick={onLogoutClicked} />
            </Link>
          </div>
        }
      </div>
    </header>
  );
};

export default AppHeader;
